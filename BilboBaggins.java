import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BilboBaggins extends Script {

    private final int SLEEPINGBAG_ID = 1263;
    private final long CHAT_TIMEOUT = 60000;
    private final long TRADE_TIMEOUT = 30000;
    private final long DENY_TRADE_CHAT_TIMEOUT = 10000;
    private final int SHOP_NPC_ID = 83;
    private final int[] SHOP_DOOR_LOC = {132, 641};
    private final String[] TEXT_MODIFIERS = {
            "@ran@",
            "@whi@",
            "@cya@",
            "@gre@"
    };
    private final String[] SAYINGS = {
            "Trade me for a free sleeping bag",
            "Sleeping bags! Get your free sleeping bag over here!"
    };
    private final String TRACKING_FILE = "bilbobaggins.txt";

    private long tradeTimeout, lastChat, lastDenyTrade;
    private String tradePartner = "";
    private int givenAmount, invTracking = 0;
    private int startX, startY;
    private String friendlyTradeTimeout;
    private boolean checkGiven = false;

    public BilboBaggins(Extension ex) {
        super(ex);
    }

    @Override
    public void init(String params) {
        startX = getX();
        startY = getY();
        friendlyTradeTimeout = String.valueOf(Math.round(TRADE_TIMEOUT/1000));
        invTracking = getInventoryCount(SLEEPINGBAG_ID);
    }

    @Override
    public int main() {

        if (isQuestMenu()) {
            answer(0);
            return random(600, 800);
        }

        if (isShopOpen()) {
            if (getInventoryCount() < 30) {
                int slot = getShopItemById(SLEEPINGBAG_ID);
                if (getShopItemAmount(slot) > 0) {
                    buyShopItem(slot, getShopItemAmount(slot));
                } else {
                    System.out.println("Bought some more bags from the shop");
                    closeShop();
                }
            } else {
                closeShop();
            }
            return 1000;
        }

        if (getInventoryCount(SLEEPINGBAG_ID) == 0) {
            if (getInventoryCount(10) < 1000) {
                System.out.println("Out of money!");
                stopScript();
                return 0;
            }
            int door = getWallObjectIdFromCoords(SHOP_DOOR_LOC[0], SHOP_DOOR_LOC[1]);
            if (door == 2) {
                System.out.println("Shop door detected as closed ... opening");
                atWallObject(SHOP_DOOR_LOC[0], SHOP_DOOR_LOC[1]);
                return random(1500, 2000);
            }
            int[] npc = getNpcByIdNotTalk(SHOP_NPC_ID);
            if (npc[0] != -1) {
                talkToNpc(npc[0]);
                return 2000;
            }
        }

        if (checkGiven) {
            checkGiven = false;
            if (getInventoryCount(SLEEPINGBAG_ID) < invTracking) {
                if (getInventoryCount(SLEEPINGBAG_ID) == (invTracking - 1)) {
                    givenAmount++;
                } else {
                    // how did we get here?!
                    System.out.println("Something happened and we're missing more bags than expected");
                    givenAmount += getInventoryCount(SLEEPINGBAG_ID);
                }
                System.out.println("Gave sleeping bag to " + tradePartner);
                addRecipientToFile(tradePartner);
                invTracking = getInventoryCount(SLEEPINGBAG_ID);
            }
        }


        if (isInTradeOffer() || isInTradeConfirm()) {
            if (System.currentTimeMillis() - tradeTimeout > TRADE_TIMEOUT) {
                System.out.println("Trading partner " + tradePartner + " failed to complete trade in time");
                say("You snooze you lose! (You have " + friendlyTradeTimeout + " seconds to complete the trade)");
                declineTrade();
                return 1000;
            }
            if (getRemoteTradeItemCount() > 0) {
                if (!(getRemoteTradeItemCount() == 1 && getRemoteTradeItemId(0) == 10)) {
                    System.out.println("Trading partner " + tradePartner + " tried to give us items!");
                    say("What part of 'free' don't you understand?!");
                    declineTrade();
                    return 1000;
                }
                // no return here - let it continue
            }
            if (isInTradeOffer()) { // trade offer screen is the first screen
                if (getLocalTradeItemCount() == 0) {
                    offerItemTrade(getInventoryIndex(SLEEPINGBAG_ID), 1);
                    return 500;
                }
                if (!hasLocalAcceptedTrade()) {
                    acceptTrade();
                }
                return 1000;
            }
            if (isInTradeConfirm()) { // trade confirm is the second screen
                if (!hasLocalAcceptedTrade()) {
                    confirmTrade();
                    checkGiven = true;
                }
                return 1000;
            }
            return 1000;
        } else {
            if (System.currentTimeMillis() - lastChat > CHAT_TIMEOUT) {
                // move our character to prevent logout
                if (!isReachable(startX, startY)) {
                    int door = getWallObjectIdFromCoords(SHOP_DOOR_LOC[0], SHOP_DOOR_LOC[1]);
                    if (door == 2) {
                        System.out.println("Shop door detected as closed ... opening");
                        atWallObject(SHOP_DOOR_LOC[0], SHOP_DOOR_LOC[1]);
                        return random(1500, 2000);
                    }
                }
                // probably need to do some isReachable checks here, but just be smart about where the script is started from
                if (getX() != startX) {
                    walkTo(startX, startY);
                } else {
                    walkTo(startX + 1, startY);
                }
                // advertise our services!
                if (random(0,9) > 5) {
                    say(TEXT_MODIFIERS[random(0,TEXT_MODIFIERS.length-1)] + SAYINGS[random(0,SAYINGS.length-1)]);
                } else {
                    say(SAYINGS[random(0,SAYINGS.length-1)]);
                }
                lastChat = System.currentTimeMillis();
                return 1000;
            }
        }
        return 1000;
    }

    @Override
    public void paint() {
        drawString("Bilbo Baggins", 25, 25, 1, 0xFFFFFF);
        drawString("Given " + givenAmount + " sleeping bags away!",
                25, 40, 1, 0xFFFFFF);
    }

    public void onTradeRequest(String name) {
        if (isInTradeConfirm() || isInTradeOffer()) { // this should never happen
            say("One moment, " + name + ". Let me finish trading " + tradePartner + " first");
            return;
        }
        tradePartner = name.toLowerCase();
        System.out.println("Receiving trade from " + name);
        if (recipientExistInFile(name)) {
            if (System.currentTimeMillis() - lastDenyTrade > DENY_TRADE_CHAT_TIMEOUT) {
                say("Sorry, " + name + ". You may only receive one sleeping bag from me.");
                lastDenyTrade = System.currentTimeMillis();
            }
            return;
        }
        sendTradeRequest(getPlayerPID(getPlayerByName(name)[0]));
        tradeTimeout = System.currentTimeMillis();
    }

    private void say(String text) {
        setTypeLine(text);
        while(!next());
    }

    private void addRecipientToFile(String recipient) {
        try {
            File file = new File(TRACKING_FILE);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file, true);
            fw.write(recipient.toLowerCase() + "\n");
            fw.flush();
            System.out.println("Added " + recipient.toLowerCase() + " to tracking file");
        } catch (Exception ex) {
            System.out.println("Failed to add recipient to tracking file");
        }
    }

    private boolean recipientExistInFile(String recipient) {
        try {
            File file = new File(TRACKING_FILE);
            if (!file.exists()) {
                System.out.println("Tracking file does not exist - allowing trade from " + recipient.toLowerCase());
                return false;
            }
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.toLowerCase().equals(recipient.toLowerCase())) {
                    br.close();
                    return true;
                }
            }
        } catch (Exception ex) {
            System.out.println("Failed search for recipient in tracking file");
        }
        return false;
    }
}
