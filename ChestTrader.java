import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.aposbot.Constants;
import com.aposbot.StandardCloseHandler;

public final class ChestTrader extends Script
        implements ActionListener, ItemListener {

   private String tradingPartner;
   private long startTime = -1L;
   private long tradeTimer = -1L;
   private long bankTimer = -1L;
   private long tradeSentTimer = -1L;
   private boolean needToMove = false;
   private final DecimalFormat iformat = new DecimalFormat("#,##0");
   private static final int CHEST_ID = 942;
   private static final Point WALKBACK_TILE = new Point(59,731);

   Panel pInput;
   private Frame frame;
   private Choice ch_GiveTake, ch_stopOnNoItemsToGive;
   private TextField tf_tradingPartner, tf_Items, tf_giveLimit;
   private Label lbl_items, lbl_limit, lbl_stopOnNoItemsToGive;

   private int[] tradeItems = new int[] {}, tradeCount = new int[] {}, giveLimit = new int[] {};
   private int itemIndex;
   private boolean giving, stopOnNoItemsToGive;
   private boolean didWithdraw = false;
   private int itemsRemaining = 0;
   private int totalCount;

   public ChestTrader(Extension ex) {
       super(ex);
   }

   @Override
   public void init(String params) {

       if (frame == null) {

           Panel pInput = new Panel(new GridLayout(0, 2, 0, 2));

           ch_GiveTake = new Choice();
           ch_GiveTake.add("Give");
           ch_GiveTake.add("Take");
           ch_GiveTake.addItemListener(this);
           pInput.add(ch_GiveTake);
           pInput.add(new Label());

           pInput.add(new Label("Name to trade:"));
           pInput.add(tf_tradingPartner = new TextField());

           lbl_items = new Label("Item ids (id1, id2, ...)");
           pInput.add(lbl_items);
           pInput.add(tf_Items = new TextField());

           lbl_limit = new Label("Giving Limits (limit1, limit2, ...)");
           pInput.add(lbl_limit);
           pInput.add(tf_giveLimit = new TextField());

           ch_stopOnNoItemsToGive = new Choice();
           ch_stopOnNoItemsToGive.add("Yes");
           ch_stopOnNoItemsToGive.add("No");
           ch_stopOnNoItemsToGive.addItemListener(this);
           lbl_stopOnNoItemsToGive = new Label("Stop script when no more items to give?");
           pInput.add(lbl_stopOnNoItemsToGive);
           pInput.add(ch_stopOnNoItemsToGive);

           Button button;
           Panel pButtons = new Panel();
           button = new Button("OK");
           button.addActionListener(this);
           pButtons.add(button);
           button = new Button("Cancel");
           button.addActionListener(this);
           pButtons.add(button);

           frame = new Frame(getClass().getSimpleName());
           frame.addWindowListener(
               new StandardCloseHandler(frame, StandardCloseHandler.HIDE)
           );
           frame.setIconImages(Constants.ICONS);
           frame.add(pInput, BorderLayout.NORTH);
           frame.add(pButtons, BorderLayout.SOUTH);
           frame.setResizable(false);
           frame.pack();
       }
       frame.setLocationRelativeTo(null);
       frame.toFront();
       frame.requestFocus();
       frame.setVisible(true);

   }

   @Override
   public int main() {

       if (startTime == -1L) {
           startTime = System.currentTimeMillis();
           frame.dispose();
       }

       if (giving && itemIndex >= tradeItems.length) {
           if (stopOnNoItemsToGive) {
               System.out.println("Done!");
               stopScript();
               return 0;
           } else {
               giving = false;
           }
       }

       if (bankTimer > -1 && !isBanking()) {
           if (System.currentTimeMillis() - bankTimer > 10000) {
               System.out.println("Triggered check for failing to open bank");
               bankTimer = -1L;
           }
           return 50;
       }

       if (isBanking()) {
           bankTimer = -1L;
           tradeTimer = -1L;
           tradeSentTimer = -1L;

           // this bit is to account for a little lag in withdrawing
           if (didWithdraw)  {
               System.out.println("Closing bank");
               didWithdraw = false;
               closeBank();
               return 600;
           }

           if (giving) {
               // script supports two givers trading
               // so if we're giving, deposit anything we aren't trying to give
               if (getInventoryCount() > 0) {
                   for (int i = 0; i < getInventoryCount(); i++) {
                       if (!inArray(tradeItems, getInventoryId(i))) {
                           //System.out.println("Identified " + getItemNameId(getInventoryId(0)) + " in inventory we need to deposit");
                           deposit(getInventoryId(0), getInventoryCount(getInventoryId(0)));
                           return 800;
                       }
                   }
               }
               // check our counts to make sure we've got enough for 2 full trades
               // eventually, this can be updated to support the giver trading
               // multiple items per trade - allowing us to get really accurate with the limits
               int bankCount = bankCount(tradeItems[itemIndex]);
               if (bankCount <= 1 || tradeCount[itemIndex] >= giveLimit[itemIndex]) {
                   if (bankCount <= 1) System.out.println("Ran out of "  + getItemNameId(tradeItems[itemIndex]));
                   else System.out.println("At or exceeded give limit of "  + getItemNameId(tradeItems[itemIndex]));
                   itemIndex++;
                   System.out.println("Moving to " + getItemNameId(tradeItems[itemIndex]));
                   return 200;
               }

               if (getInventoryCount() < 24) {
                   int need = 0;
                   if (isItemStackableId(tradeItems[itemIndex])) {
                       need = Math.min(bankCount - 1, giveLimit[itemIndex]);
                   } else {
                       need = Math.min(bankCount - 1, Math.min(24 - getInventoryCount(), giveLimit[itemIndex] - tradeCount[itemIndex] - getInventoryCount(tradeItems[itemIndex])));
                   }
                   if (need == 0) {
                       itemIndex++;
                       return 200;
                   }

                   System.out.println("Withdrawing " + need + " of " + getItemNameId(tradeItems[itemIndex]));
                   withdraw(tradeItems[itemIndex], need);

                   // only update items remaining once per bank sessions
                   int tmpItemsRemaining = 0;
                   for (int i = itemIndex; i < tradeItems.length; i++) {
                       if (giveLimit[i] < Integer.MAX_VALUE) {
                           tmpItemsRemaining += Math.min(giveLimit[i] - tradeCount[i], bankCount(tradeItems[i]) - 1);
                       } else {
                           tmpItemsRemaining += bankCount(tradeItems[i]) - 1;
                       }
                   }
                   itemsRemaining = tmpItemsRemaining;

                   if (getInventoryCount() + need >= 24) didWithdraw = true;
                   if (need < 24) {
                       return 1800;
                   }
                   return 600;
               } else {
                   closeBank();
                   return 600;
               }
           } else {
               // taker just deposits everything given to it
               if (getInventoryCount() > 0) {
                   System.out.println("Identified " + getItemNameId(getInventoryId(0)) + " in inventory we need to deposit");
                   deposit(getInventoryId(0), getInventoryCount(getInventoryId(0)));
                   return 600;
               } else {
                   closeBank();
                   return 600;
               }
           }
       }

       if (isInTradeConfirm()) {
           tradeSentTimer = -1L;
           bankTimer = -1L;
           if (tradeTimer == -1L) {
               tradeTimer = System.currentTimeMillis();
           } else {
               if (System.currentTimeMillis() - tradeTimer > 10000) {
                   System.out.println("Triggered check for trade taking too long");
                   tradeTimer = -1L;
                   declineTrade();
                   return 600;
               }
           }
           confirmTrade();
           return 600;
       }

       if (isInTradeOffer()) {
           tradeSentTimer = -1L;
           bankTimer = -1L;
           if (tradeTimer == -1L) {
               tradeTimer = System.currentTimeMillis();
           } else {
               if (System.currentTimeMillis() - tradeTimer > 10000) {
                   System.out.println("Triggered check for trade taking too long");
                   declineTrade();
                   return 600;
               }
           }
           if (giving) {
               if (getLocalTradeItemCount() < 12) {
                   for (int i = 0; i < 24; i++) {
                       if (inArray(tradeItems, getInventoryId(i)) && !isItemInTradeOffer(getInventoryId(i))) {
                           if (isItemStackableId(getInventoryId(i))) {
                               System.out.println("Offering " + getInventoryCount(getInventoryId(i)) + " of item " + getItemNameId(getInventoryId(i)));
                               offerItemTrade(i, getInventoryCount(getInventoryId(i)));
                           } else {
                               System.out.println("Offering " + Math.max(12 - getLocalTradeItemCount(), getInventoryCount(getInventoryId(i))) + " of item " + getItemNameId(getInventoryId(i)));
                               offerItemTrade(i, Math.min(12 - getLocalTradeItemCount(), getInventoryCount(getInventoryId(i))));
                           }
                           return 600;
                       }
                   }
               }
           }
           System.out.println("Accepting trade");
           acceptTrade();
           return 600;
       }

       tradeTimer = -1L;

       if (needToMove) {
           needToMove = false;
           bankTimer = -1L;
           tradeSentTimer = -1L;
           tradeTimer = -1L;
           if (isReachable(getX() + 1, getY())) {
               walkTo(getX()+1,getY());
               return 1500;
           }
           if (isReachable(getX(), getY()+1)) {
               walkTo(getX(),getY()+1);
               return 1500;
           }
           if (isReachable(getX(), getY()-1)) {
               walkTo(getX(),getY()-1);
               return 1500;
           }
           if (isReachable(getX()-1, getY())) {
               walkTo(getX()-1,getY());
               return 1500;
           }
       }

       int[] chest = getObjectById(CHEST_ID);
       if (chest[0] != -1 && distanceTo(chest[1], chest[2]) > 2) {
           System.out.println("We got more than 2 tile from the chest");
           walkTo(WALKBACK_TILE.x, WALKBACK_TILE.y);
           bankTimer = -1L;
           tradeSentTimer = -1L;
           tradeTimer = -1L;
           return 1800;
       }

       if (!giving) { // taking
           if (getInventoryCount() > 12) {
               if (chest[0] != -1) {
                   atObject(chest[1], chest[2]);
                   return 600;
               }
               return 300;
           } else {
               int[] player = getPlayerByName(tradingPartner);
               if (player[0] != -1) {
                   if (tradeSentTimer == -1L) {
                       tradeSentTimer = System.currentTimeMillis();
                       sendTradeRequest(getPlayerPID(player[0]));
                       return 600;
                   } else {
                       if (System.currentTimeMillis() - tradeSentTimer > 3000) {
                           tradeSentTimer = System.currentTimeMillis();
                           sendTradeRequest(getPlayerPID(player[0]));
                           return 600;
                       } else {
                           return 50;
                       }
                   }
               }
           }
       } else { // giving
           if (getInventoryCount(tradeItems) < 12) {
               if (chest[0] != -1) {
                   atObject(chest[1], chest[2]);
                   return 500;
               }
               return 300;
           } else {
               int[] player = getPlayerByName(tradingPartner);
               if (player[0] != -1) {
                   if (tradeSentTimer == -1L) {
                       tradeSentTimer = System.currentTimeMillis();
                       sendTradeRequest(getPlayerPID(player[0]));
                       return 600;
                   } else {
                       if (System.currentTimeMillis() - tradeSentTimer > 3000) {
                           System.out.println("Triggered check for not in trade");
                           sendTradeRequest(getPlayerPID(player[0]));
                           return 600;
                       } else {
                           return 50;
                       }
                   }
               }
           }
       }
       return 1000;
   }

   public boolean isItemInTradeOffer(int id) {
       if (!isInTradeOffer()) return false;
       for (int i = 0; i < 12; i++) {
           if (getLocalTradeItemId(i) == id) return true;
       }
       return false;
   }

   @Override
   public void onServerMessage(String str) {
       str = str.toLowerCase();
       if (str.contains("this chest")) {
           bankTimer = System.currentTimeMillis();
           tradeTimer = -1L;
           tradeSentTimer = -1L;
       } else if (str.contains("have been standing")) {
           needToMove = true;
       } else if (str.contains("trade completed")) {
           tradeTimer = -1L;
           tradeSentTimer = -1L;
           totalCount += 12;
           if (giving) {
               tradeCount[itemIndex] += 12;
           }
       } else if (str.contains("declined")) {
           tradeTimer = -1L;
           tradeSentTimer = -1L;
       }
   }

   // ripped from Shantay_Trader
   private String per_hour(long count, long time) {
       double amount, secs;

       if (count == 0) return "0";
       amount = count * 60.0 * 60.0;
       secs = (System.currentTimeMillis() - time) / 1000.0;
       return iformat.format(amount / secs);
   }

   @Override
   public void paint() {
       final int white = 0xFFFFFF;
       final int cyan = 0x00FFFF;
       int y = 25;
        int num = 0;
       drawString("Shantay Chest Trader", 25, y, 1, white);
       y += 15;
       drawString("Runtime: " + get_time_since(startTime), 25, y, 1, white);
       y += 15;
       if (giving) {
           drawString("Remaining Items: " + itemsRemaining, 25, y, 1, white);
           y += 15;
       }
       drawString("Transfer rate: " + per_hour(totalCount, startTime) + "/h", 25, y, 1, white);
       y += 15;
   }

   // ripped from somewhere.. one of the S_ scripts
    private static String get_time_since(long t) {
        long millis = (System.currentTimeMillis() - t) / 1000;
        long second = millis % 60;
        long minute = (millis / 60) % 60;
        long hour = (millis / (60 * 60)) % 24;
        long day = (millis / (60 * 60 * 24));

        if (day > 0L) {
            return String.format("%02d days, %02d hrs, %02d mins",
                    day, hour, minute);
        }
        if (hour > 0L) {
            return String.format("%02d hours, %02d mins, %02d secs",
                    hour, minute, second);
        }
        if (minute > 0L) {
            return String.format("%02d minutes, %02d seconds",
                    minute, second);
        }
        return String.format("%02d seconds", second);
    }

   @Override
    public void actionPerformed(ActionEvent aE) {
        if (aE.getActionCommand().equals("OK")) {
            String[] items;
            tradingPartner = tf_tradingPartner.getText();
            System.out.println("Trading partner is " + tradingPartner);
            if (ch_GiveTake.getSelectedItem() == "Give") {
                System.out.println("Option selected: GIVING");
                giving = true;
            } else if(ch_GiveTake.getSelectedItem() == "Take") {
                System.out.println("Option selected: TAKING");
                giving  = false;
            }
            if (giving) {
                items = tf_Items.getText().trim().split(",");
                tradeItems = new int[items.length];
                tradeCount = new int[items.length];
                String itemStr = "";
                for (int i = 0; i < items.length; i++) {
                    itemStr += getItemNameId(Integer.parseInt(items[i])) + ", ";
                    tradeItems[i] = Integer.parseInt(items[i]);
                }
                System.out.println("Giving the following items: " + itemStr.substring(0, itemStr.length()-2));
                giveLimit = new int[tradeItems.length];
                if (!tf_giveLimit.getText().trim().equals("")) {
                    String[] limits = tf_giveLimit.getText().trim().split(",");
                    if (limits.length != items.length) {
                        System.out.println("If specifying limit, you need a limit for each item giving");
                        System.out.println("Incorrect number of limit for the number of items giving");
                        stopScript();
                        return;
                    }
                    for (int i = 0; i < limits.length; i++) {
                        if (Integer.parseInt(limits[i]) <= 0 || limits[i].trim().equals("")) {
                            giveLimit[i] = Integer.MAX_VALUE;
                        } else {
                            giveLimit[i] = Integer.parseInt(limits[i]);
                        }
                        System.out.println("Giving limit of " + giveLimit[i] + " for " + getItemNameId(tradeItems[i]));
                    }
                } else {
                    System.out.println("No give limits");
                    for (int i = 0; i < tradeItems.length; i++) {
                        giveLimit[i] = Integer.MAX_VALUE;
                    }
                }
                if (ch_stopOnNoItemsToGive.getSelectedItem() == "Yes") {
                    System.out.println("Option selected: Stopping when there are no more items to give");
                    stopOnNoItemsToGive = true;
                } else if(ch_stopOnNoItemsToGive.getSelectedItem() == "No") {
                    System.out.println("Option selected: Continuing to trade when no more items to give");
                    stopOnNoItemsToGive  = false;
                }


            }
            tradingPartner = tf_tradingPartner.getText().trim();
        }
        frame.setVisible(false);
    }

    @Override
    public void itemStateChanged(ItemEvent iE) {
        switch(ch_GiveTake.getSelectedItem()) {
            case "Give": {
                tf_Items.setVisible(true);
                lbl_items.setVisible(true);
                lbl_limit.setVisible(true);
                lbl_stopOnNoItemsToGive.setVisible(true);
                ch_stopOnNoItemsToGive.setVisible(true);
                tf_giveLimit.setVisible(true);
                tf_Items.getParent().revalidate();
                tf_Items.repaint();
                break;
            }
            case "Take": {
                tf_Items.setVisible(false);
                lbl_items.setVisible(false);
                lbl_limit.setVisible(false);
                lbl_stopOnNoItemsToGive.setVisible(false);
                ch_stopOnNoItemsToGive.setVisible(false);
                tf_giveLimit.setVisible(false);
                tf_Items.getParent().revalidate();
                tf_Items.repaint();
                break;
            }
            default: {
                stopScript();
                break;
            }
        }
    }
}
