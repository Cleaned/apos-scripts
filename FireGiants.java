import com.aposbot.Constants;
import com.aposbot.StandardCloseHandler;

import java.awt.*;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

public class FireGiants extends Script implements ActionListener {

    // item ids
    private final int
            BIG_BONES = 413,
            SHARK = 546,
            VIAL = 465,
            GLARIALS_AMMY = 782,
            ROPE = 237,
            LOBSTER = 373,
            AIR_RUNES = 33,
            LAW_RUNES = 42;

    // item groups
    private final int[] SUPER_ATTACK = new int[] { 486, 487, 488 };
    private final int[] SUPER_STRENGTH = new int[] { 492, 493, 494 };
    private final int[] SUPER_DEFENSE = new int[] { 495, 496, 497 };
    private final int[][] POTIONS = new int[][] { SUPER_ATTACK, SUPER_DEFENSE, SUPER_STRENGTH };
    private final int[][] RUNES = new int[][]{
            new int[] {AIR_RUNES, 5},
            new int[] {LAW_RUNES, 1}
    };

    // npc ids
    private final int FIRE_GIANT = 344;

    // game object ids
    private final int
            RAFT = 464,
            TREE1 = 462,
            TREE2 = 463,
            TREE3 = 482;

    // pathing/state definitions
    private PathWalker.Path TO_BANK, TO_FALLS;
    private final Point CAMELOT_TP = new Point(456 ,456);
    private final Point WATERFALL = new Point(659,449);
    private final Point SEERS_BANK = new Point(501,450);
    private final Point FG_ROOM_NORTH = new Point(660,3288);
    private final Point FG_ROOM_WEST = new Point(668, 3294);

    // everything to pick up
    private final int[] PICKUP = new int[] { 615, 619, 38, 40, 42, 438, 439, 440, 441, 442, 443, 795, 1277, 81, 404, 405, 93, 400, 402, 403, 408, 523, 526, 527, 398, 373 };
    // priorty pickup will eat food to pick an item up
    private final int[] PRIORITY_PICKUP = new int[] { 795, 1277, 81, 404, 405, 93, 400, 402, 403, 408, 523, 526, 527, 398};
    // needed for banking anything dropped that's also part of our normal equipment
    private final ArrayList<Integer> EQUIPPED = new ArrayList<Integer>();
    private int AMULET_ID;

    // tracking
    private final DecimalFormat iformat = new DecimalFormat("#,##0");
    private long startTime = -1;
    private PathWalker pw;
    private int[] startXp = new int[] { 0, 0 }; // { combat,  pray }
    private int[] currentXp = new int[] { 0, 0}; // { combat,  pray }
    private boolean countKill = false, tpCheck = false;
    private int kills = 0, trips = 0;
    private int lastX = 0, lastY = 0;

    // form fields
    private Frame frame;
    private TextField tf_eatAt, tf_eatAtEmergency;
    private Choice ch_room, ch_buryBones, ch_useSupers;

    // configurable
    private int eatAt;
    private int eatAtEmergency;
    private boolean buryBones, useSupers;
    private int room; // = 0 north, 1 = west


    public FireGiants(Extension ex) {
        super(ex);
        pw = new PathWalker(ex);
    }

    @Override
    public void init(String params) {
        pw.init(null);
        TO_BANK = pw.calcPath(CAMELOT_TP.x, CAMELOT_TP.y, SEERS_BANK.x, SEERS_BANK.y);
        TO_FALLS = pw.calcPath(SEERS_BANK.x, SEERS_BANK.y, WATERFALL.x, WATERFALL.y);

        if (getInventoryCount(GLARIALS_AMMY, ROPE) < 2) {
            System.out.println("Could not find either rope or glarial's amulet. Killing script");
            stopScript();
            setAutoLogin(false);
            return;
        }

        // need to get what item's we're wearing for switching ammies and banking loot that may overlap with equipment
        for (int i = 0; i < MAX_INV_SIZE; i++) {
            if (isItemEquipped(i) || getItemName(i).toLowerCase(Locale.ENGLISH).contains("amulet")) {
                if (getInventoryId(i) == GLARIALS_AMMY) continue;
                System.out.println("Identified equipment item " + getItemName(i));
                EQUIPPED.add(getInventoryId(i));
                if (getItemName(i).toLowerCase(Locale.ENGLISH).contains("amulet")) {
                    System.out.println("Identified amulet " + getItemName(i));
                    AMULET_ID = getInventoryId(i);
                }
            }
        }
        EQUIPPED.add(LAW_RUNES);

        eatAt = getLevel(3) - 25;

        if (frame == null) {

            Panel pInput = new Panel(new GridLayout(0, 2, 0, 2));

            // Room selection
            ch_room = new Choice();
            ch_room.add("North");
            ch_room.add("West");
            pInput.add(new Label("Room"));
            pInput.add(ch_room);

            // Eating options
            pInput.add(new Label("Eat At"));
            tf_eatAt = new TextField();
            pInput.add(tf_eatAt);
            tf_eatAt.setText(Integer.toString(getLevel(3) - 25));
            pInput.add(new Label("Emergency Eating"));
            tf_eatAtEmergency = new TextField();
            pInput.add(tf_eatAtEmergency);
            tf_eatAtEmergency.setText("20");

            // Other options
            ch_useSupers = new Choice();
            ch_useSupers.add("Yes");
            ch_useSupers.add("No");
            pInput.add(new Label("Use Super Potions"));
            pInput.add(ch_useSupers);

            ch_buryBones = new Choice();
            ch_buryBones.add("Yes");
            ch_buryBones.add("No");
            pInput.add(new Label("Bury Bones"));
            pInput.add(ch_buryBones);


            Button button;
            Panel pButtons = new Panel();
            button = new Button("OK");
            button.addActionListener(this);
            pButtons.add(button);
            button = new Button("Cancel");
            button.addActionListener(this);
            pButtons.add(button);

            frame = new Frame(getClass().getSimpleName());
            frame.addWindowListener(
                    new StandardCloseHandler(frame, StandardCloseHandler.HIDE)
            );
            frame.setIconImages(Constants.ICONS);
            frame.add(pInput, BorderLayout.NORTH);
            frame.add(pButtons, BorderLayout.SOUTH);
            frame.setResizable(false);
            frame.pack();
        }
        frame.setLocationRelativeTo(null);
        frame.toFront();
        frame.requestFocus();
        frame.setVisible(true);
    }

    @Override
    public int main() {

        if (startTime == -1) {
            startTime = System.currentTimeMillis();
            startXp[0] = getXpForLevel(0) +  getXpForLevel(1) +  getXpForLevel(2);
            startXp[1] =  getXpForLevel(5);
            frame.dispose();
        }

        currentXp[0] = getXpForLevel(0) +  getXpForLevel(1) +  getXpForLevel(2);
        currentXp[1] = getXpForLevel(5);

        if (pw.walkPath()) return 1000;

        if (isQuestMenu()) {
            answer(0);
            return 4000;
        }

        if (isBanking()) {
            if (getInventoryCount() == MAX_INV_SIZE && getInventoryCount(SHARK) > 0) {
                System.out.println("Done banking");
                closeBank();
                return 600;
            }
            if (getInventoryCount(PICKUP) > 0) {
                for (int id : PICKUP) {
                    if (getInventoryCount(id) > 0) {
                        // if we pick up something we equip, deposit all but 1
                        if (EQUIPPED.contains(id)) {
                            if (getInventoryCount(id) > 1) {
                                System.out.println("Depositing " + getItemNameId(id) + " quantity " + (getInventoryCount(id)-1));
                                deposit(id, getInventoryCount(id) - 1);
                                return 1000;
                            }
                        } else {
                            System.out.println("Depositing " + getItemNameId(id) + " quantity " + getInventoryCount(id));
                            deposit(id, getInventoryCount(id));
                            return 1000;
                        }
                    }
                }
            }

            if (getInventoryCount(ROPE) == 0) {
                if (bankCount(ROPE) > 0) {
                    withdraw(ROPE, 1);
                    return 1600;
                } else {
                    stopScript();
                    System.out.println("No more ropes!");
                    return 0;
                }
            }

            if (useSupers) {
                // withdraw the highest dose of a potion we have
                // and deposit anything of lower dose
                for (int[] potion : POTIONS) {
                    if (bankCount(potion[0]) > 0 && getInventoryCount(potion[0]) < 1) {
                        if (getInventoryCount(potion[1]) > 0) {
                            System.out.println("Depositing " + getItemNameId(potion[1]));
                            deposit(potion[1], getInventoryCount(potion[1]));
                            return 800;
                        }
                        if (getInventoryCount(potion[2]) > 0 ) {
                            System.out.println("Depositing " + getItemNameId(potion[2]));
                            deposit(potion[2], getInventoryCount(potion[2]));
                            return 800;
                        }
                        if (getInventoryCount(potion[0]) < 1) {
                            System.out.println("Withdrawing 1 " + getItemNameId(potion[0]));
                            withdraw(potion[0], 1);
                            return 1600;
                        }
                    } else if (bankCount(potion[1]) > 0  && getInventoryCount(potion[0]) < 1 && getInventoryCount(potion[1]) < 1) {
                        if (getInventoryCount(potion[2]) > 0) {
                            System.out.println("Depositing " + getItemNameId(potion[2]));
                            deposit(potion[2], getInventoryCount(potion[2]));
                            return 800;
                        }
                        if (getInventoryCount(potion[1]) < 1) {
                            System.out.println("Withdrawing 1 " + getItemNameId(potion[1]));
                            withdraw(potion[1], 1);
                            return 1600;
                        }
                    } else if (bankCount(potion[2]) > 0 && getInventoryCount(potion[0]) < 1 && getInventoryCount(potion[1]) < 1 && getInventoryCount(potion[2]) < 1) {
                        if (getInventoryCount(potion[2]) < 1) {
                            System.out.println("Withdrawing 1 " + getItemNameId(potion[2]));
                            withdraw(potion[2], 1);
                            return 1600;
                        }
                    }
                }
            }

            for (int[] rune : RUNES) {
                if (getInventoryCount(rune[0]) == 0) {
                    if (bankCount(rune[0]) < rune[1]) {
                        System.out.println("Unable to withdraw " + rune[1] + " of " + getItemNameId(rune[0]));
                        stopScript();
                        setAutoLogin(false);
                        return 0;
                    }
                    System.out.println("Withdrawing " + rune[1] + " " + getItemNameId(rune[0]));
                    withdraw(rune[0], rune[1]);
                    return 1200;
                }
            }

            System.out.println("Withdrawing " + (MAX_INV_SIZE - getInventoryCount()) + " shark");
            if (bankCount(SHARK) < MAX_INV_SIZE - getInventoryCount()) {
                System.out.println("Out of food!");
                stopScript();
                setAutoLogin(false);
                return 0;
            } else {
                withdraw(SHARK, MAX_INV_SIZE - getInventoryCount());
                return 1200;
            }
        }

        if (isUnderground()) {
            if (isAtFireGiants(getX(), getY(), room)) {
                if (checkEquipItem(AMULET_ID)) {
                    return 1200;
                }
                if (getInventoryCount(new int[] {SHARK,LOBSTER}) == 0) {
                    // out of food
                    // pick up anything on ground and book it out of there
                    int[] item = getGroundItems();
                    if (item[0] != -1) {
                        // item is on ground
                        // leave combat and get it
                        tpCheck = false;
                        if (inCombat()) {
                            walkTo(item[1], item[2]);
                            countKill = false;
                            return 600;
                        } else {
                            System.out.println("Picking up item " + getItemNameId(item[0]) + " before teleport out");
                            pickupItem(item[0], item[1], item[2]);
                            return 1200;
                        }
                    } else {
                        // teleport out
                        if (canCastSpell(22) ) {
                            // wait 2 seconds before teleporting for ground items to appear
                            if (!tpCheck) {
                                tpCheck = true;
                                return 2000;
                            }
                            System.out.println("Teleporting out!");
                            castOnSelf(22);
                            tpCheck = true;
                            trips++;
                            return 3000;
                        } else {
                            // something went horribly wrong
                            System.out.println("No runes to cast teleport!?");
                            stopScript();
                            setAutoLogin(false);
                            return 0;
                        }

                    }
                } else {
                    // run away if too low hp for eating
                    if (inCombat()) {
                        if (getCurrentLevel(3) <= eatAtEmergency) {
                            countKill = false;
                            walkTo(getX(), getY());
                            return 600;
                        }
                        int[] item = getGroundItems();
                        if (inArray(PRIORITY_PICKUP, item[0])) {
                            countKill = false;
                            walkTo(item[1], item[2]);
                            return 600;
                        } else {
                            countKill = true;
                            return 50;
                        }
                    }

                    if (countKill) {
                        kills++;
                        countKill = false;
                    }

                    // assume not in combat below this point
                    if (getCurrentLevel(3) < eatAt) {
                        if (getInventoryCount(SHARK, LOBSTER) > 0) {
                            System.out.println("Eating");
                            // prioritize lobsters to be eaten first
                            if (getInventoryCount(LOBSTER) > 0) {
                                useItem(getInventoryIndex(LOBSTER));
                                return 1200;
                            } else {
                                useItem(getInventoryIndex(SHARK));
                                return 1200;
                            }
                        }
                    }

                    // bone bury
                    if (buryBones && getInventoryCount(BIG_BONES) > 0) {
                        useItem(getInventoryIndex(BIG_BONES));
                        return 1200;
                    }

                    // drop vials if we've got any
                    if (getInventoryCount(VIAL) > 0) {
                        System.out.println("Dropping vial");
                        dropItem(getInventoryIndex(VIAL));
                        return 1200;
                    }

                    int[] item = getGroundItems();
                    if (item[0] != -1) {
                        System.out.println("Picking up " + getItemNameId(item[0]));
                        pickupItem(item[0], item[1], item[2]);
                        return 1200;
                    }

                    int sipId = needToSip();
                    if (sipId >= 0) {
                        doSip(sipId);
                        return 1200;
                    }

                    if (getFatigue() > 50) {
                        // only sleep if we're gonna be here awhile
                        if (getInventoryCount(SHARK) > 4) {
                            useSleepingBag();
                            return 1000;
                        }
                    }

                    int[] npc = getNearestNpc(FIRE_GIANT);
                    if (npc[0] != -1) {
                        if (distanceTo(npc[1], npc[2]) <= 8) {
                            System.out.println("Attacking fire giant");
                            attackNpc(npc[0]);
                            return 1000;
                        }
                    }

                }
            } else {
                return getToGiantsUnderground(room);
            }
        } else {
            // above ground
            if (buryBones && getInventoryCount(BIG_BONES) > 0) {
                useItem(getInventoryIndex(BIG_BONES));
                return 1200;
            }

            if (checkEquipItem(GLARIALS_AMMY)) {
                return 1200;
            }
            if (distanceTo(CAMELOT_TP.x, CAMELOT_TP.y) < 5) {
                if (getFatigue() > 0) {
                    useSleepingBag();
                    return 1000;
                }
                System.out.println("Walking to bank");
                pw.setPath(TO_BANK);
                return 1000;
            }
            if (distanceTo(SEERS_BANK.x, SEERS_BANK.y) < 7) {
                return handleSeersBank();
            }
            if (distanceTo(WATERFALL.x, WATERFALL.y) < 5) {
                int[] raft = getObjectById(RAFT);
                if (raft[0] != -1) {
                    System.out.println("Getting on raft");
                    atObject(raft[1], raft[2]);
                    return 3000;
                }
            }

            if (getY() == 463 && getX() == 662) {
                System.out.println("Rope on tree 1");
                useItemOnObject(ROPE,TREE1);
                return 3000;
            }

            if (getX() == 662 && getY() == 467) {
                System.out.println("Rope on tree 2");
                useItemOnObject(ROPE,TREE2);
                return 3000;
            }

            if (getX() == 659 && getY() == 471) {
                System.out.println("Rope on tree 3");
                useItemOnObject(ROPE,TREE3);
                return 3000;
            }
        }

        return 600;
    }

    public int handleSeersBank() {
        if (getInventoryCount(SHARK) > 0 && getInventoryCount() == MAX_INV_SIZE) {
            if (getLevel(3) - getCurrentLevel(3) >= 10) {
                System.out.println("Hp a little low, so eating a shark while at bank");
                useItem(getInventoryIndex(SHARK));
                return 1200;
            }
            System.out.println("Walking to waterfall");
            pw.setPath(TO_FALLS);
            return 1000;
        } else  {
            // banking
            int[] banker = getNpcByIdNotTalk(BANKERS);
            if (banker[0] != -1) {
                talkToNpc(banker[0]);
                return 3000;
            }
        }
        return 600;
    }

    public int getToGiantsUnderground(int room) {
        if (inCombat()) {
            lastX = -1;
            lastY = -1;
            walkTo(getX(), getY());
            return 600;
        }
        if (getY() >= 3303) {
            if (checkEquipItem(GLARIALS_AMMY)) {
                return 1200;
            }
            int[] doors = getObjectById(471);
            if (doors[0] != -1) {
                System.out.println("Entering waterfall main doors");
                lastX = -1;
                lastY = -1;
                atObject(doors[1], doors[2]);
                return 3000;
            }
        }

        if (room == 0) {
            if (isReachable(FG_ROOM_NORTH.x, FG_ROOM_NORTH.y)) {
                if (lastX == getX() && lastY == getY()) {
                    for(int i = 0; i < 794; i++) {
                        int[] npc = getNpcInRadius(i, getX(), getY(), 1);
                        if (npc[0] >= 0) {
                            System.out.println("Attacking blocking npc " + getNpcName(npc[0]));
                            attackNpc(npc[0]);
                            return 1200;
                        }
                    }
                    lastX = -1;
                    lastY = -1;
                } else {
                    lastX = getX();
                    lastY = getY();
                    walkTo(FG_ROOM_NORTH.x, FG_ROOM_NORTH.y);
                    return 1800;
                }
            }  else {
                getObjectIdFromCoords(659,3289);
                if (getObjectIdFromCoords(659,3289) == 64) {
                    if (lastX == getX() && lastY == getY()) {
                        for(int i = 0; i < 794; i++) {
                            int[] npc = getNpcInRadius(i, getX(), getY(), 1);
                            if (npc[0] >= 0) {
                                    System.out.println("Attacking blocking npc " + getNpcName(npc[0]));
                                    attackNpc(npc[0]);
                                    return 1200;
                            }
                        }
                        lastX = -1;
                        lastY = -1;
                    } else {
                        lastX = getX();
                        lastY = getY();
                        System.out.println("Opening closed doors");
                        atObject(659, 3289);
                        return 1800;
                    }
                }
            }
        } else {
            if (isReachable(FG_ROOM_WEST.x, FG_ROOM_WEST.y)) {
                if (lastX == getX() && lastY == getY()) {
                    for(int i = 0; i < 794; i++) {
                        int[] npc = getNpcInRadius(i, getX(), getY(), 1);
                        if (npc[0] >= 0) {
                            System.out.println("Attacking blocking npc " + getNpcName(npc[0]));
                            attackNpc(npc[0]);
                            return 1200;
                        }
                    }
                    lastX = -1;
                    lastY = -1;
                } else {
                    lastX = getX();
                    lastY = getY();
                    System.out.println("Opening closed doors");
                    walkTo(FG_ROOM_WEST.x, FG_ROOM_WEST.y);
                    return 1800;
                }
            }  else {
                if (getObjectIdFromCoords(663, 3297) == 2) {
                    if (lastX == getX() && lastY == getY()) {
                        for(int i = 0; i < 794; i++) {
                            int[] npc = getNpcInRadius(i, getX(), getY(), 1);
                            if (npc[0] >= 0) {
                                System.out.println("Attacking blocking npc " + getNpcName(npc[0]));
                                attackNpc(npc[0]);
                                return 1200;
                            }
                        }
                        lastX = -1;
                        lastY = -1;
                    } else {
                        lastX = getX();
                        lastY = getY();
                        System.out.println("Opening closed doors");
                        atObject(663, 3297);
                        return 1800;
                    }
                }
                if (getObjectIdFromCoords(667, 3295) == 64) {
                    if (lastX == getX() && lastY == getY()) {
                        for(int i = 0; i < 794; i++) {
                            int[] npc = getNpcInRadius(i, getX(), getY(), 1);
                            if (npc[0] >= 0) {
                                System.out.println("Attacking blocking npc " + getNpcName(npc[0]));
                                attackNpc(npc[0]);
                                return 1200;
                            }
                        }
                        lastX = -1;
                        lastY = -1;
                    } else {
                        lastX = getX();
                        lastY = getY();
                        System.out.println("Opening closed doors");
                        atObject(667, 3295);
                        return 1800;
                    }
                }
            }
        }
        return 1000;
    }

    public boolean checkEquipItem(int id) {
        if (!isItemEquipped(getInventoryIndex(id))) {
            System.out.println("Switching to " + getItemNameId(id));
            wearItem(getInventoryIndex(id));
            return true;
        }
        return false;
    }

    public int needToSip() {
        if (!useSupers) return -1;
        if (getInventoryCount(SHARK) <= 3) return -1;
        if (getCurrentLevel(0) - getLevel(0) <= 3 && getInventoryCount(SUPER_ATTACK) > 0) return 0;
        if (getCurrentLevel(1) - getLevel(1) <= 3 && getInventoryCount(SUPER_DEFENSE) > 0) return 1;
        if (getCurrentLevel(2) - getLevel(2) <= 3 && getInventoryCount(SUPER_STRENGTH) > 0) return 2;
        return -1;
    }

    private void doSip(int pot) {
        if (!useSupers) return;
        switch(pot) {
            case 0: {
                if (getInventoryCount(SUPER_ATTACK) > 0) {
                    System.out.println("Drinking super attack");
                    useItem(getInventoryIndex(SUPER_ATTACK));
                    return;
                }
                break;
            }
            case 1: {
                if (getInventoryCount(SUPER_DEFENSE) > 0) {
                    System.out.println("Drinking super defense");
                    useItem(getInventoryIndex(SUPER_DEFENSE));
                    return;
                }
                break;
            }
            case 2: {
                if (getInventoryCount(SUPER_STRENGTH) > 0) {
                    System.out.println("Drinking super strength");
                    useItem(getInventoryIndex(SUPER_STRENGTH));
                    return;
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    private boolean isUnderground() {
        if (getY() > 3000) return true;
        return false;
    }

    private boolean isAtFireGiants(int x, int y, int room) {
        if (room == 0) {
            return (y <= 3288);
        }
        else {
            return (x <= 670 && x >= 663 && y >= 3290 && y <= 3294);
        }
    }

    private int[] getGroundItems() {
        int[] item = new int[] {
                -1, -1, -1
        };
        int count = getGroundItemCount();
        int max_dist = Integer.MAX_VALUE;
        for (int i = 0; i < count; i++) {
            int id = getGroundItemId(i);
            if (inArray(PICKUP, id) || (buryBones && id == BIG_BONES)) {
                int x = getItemX(i);
                int y = getItemY(i);
                if (!isReachable(x, y)) continue;
                if (!isAtFireGiants(x, y, room)) continue;
                // if inventory full, pick up stackables if we already have them in inventory
                // if it's a priority pickup, eat a shark or drop attack potion
                if (getInventoryCount() == MAX_INV_SIZE) {
                    if (inArray(PRIORITY_PICKUP, id)) {
                        if (getInventoryCount(LOBSTER) > 0) {
                            System.out.println("Eating shark to make room for priority item " + getItemNameId(id));
                            useItem(getInventoryIndex(LOBSTER));
                            return new int[] { -1, -1, -1};
                        }
                        if (getInventoryCount(SHARK) > 0) {
                            System.out.println("Eating shark to make room for priority item " + getItemNameId(id));
                            useItem(getInventoryIndex(SHARK));
                            return new int[] { -1, -1, -1};
                        }
                    } else {
                        // dont try to pick up unstackable non-priority items
                        if (!isItemStackableId(id)) {
                            continue;
                        } else {
                            // if they're stackable, only pick if we already have in inventory
                            if (getInventoryCount(id) == 0) {
                                continue;
                            }
                        }
                    }
                }
                int dist = distanceTo(x, y, getX(), getY());
                if (dist < max_dist) {
                    item[0] = id;
                    item[1] = x;
                    item[2] = y;
                    max_dist = dist;
                }
            }
        }
        return item;
    }

    private int[] getNearestNpc(int id) {
        int[] attack = new int[] { -1, -1, -1};
        int mindist = Integer.MAX_VALUE;
        int count = countNpcs();
        for (int i = 0; i < count; i++) {
            if (isNpcInCombat(i)) continue;
            if (!isAtFireGiants(getNpcX(i), getNpcY(i), room)) continue;
            if (getNpcId(i) == id) {
                int y = getNpcY(i);
                int x = getNpcX(i);
                int dist = distanceTo(x, y, getX(), getY());
                if (dist < mindist) {
                    attack[0] = i;
                    attack[1] = x;
                    attack[2] = y;
                    mindist = dist;
                }
            }
        }
        return attack;
    }

    @Override
    public void paint() {
        final int white = 0xFFFFFF;
        final int cyan = 0x00FFFF;
        int y = 25;
        drawString("Fire Giants", 190, y, 1, cyan);
        y += 15;
        drawString("Runtime: " + get_time_since(startTime), 190, y, 1, white);
        y += 15;
        drawString("Trips: " + trips, 190, y, 1, white);
        y += 15;
        drawString("Kills: " + kills, 190, y, 1, white);
        y += 15;
        drawString("Combat Xp/Hr: " + per_hour(currentXp[0] - startXp[0], startTime), 190, y, 1, white);
        y += 15;
        drawString("Prayer Xp/Hr: " + per_hour(currentXp[1] - startXp[1], startTime), 190, y, 1, white);
    }

    // ripped from somewhere.. one of the S_ scripts
    private static String get_time_since(long t) {
        long millis = (System.currentTimeMillis() - t) / 1000;
        long second = millis % 60;
        long minute = (millis / 60) % 60;
        long hour = (millis / (60 * 60)) % 24;
        long day = (millis / (60 * 60 * 24));

        if (day > 0L) {
            return String.format("%02d days, %02d hrs, %02d mins",
                    day, hour, minute);
        }
        if (hour > 0L) {
            return String.format("%02d hours, %02d mins, %02d secs",
                    hour, minute, second);
        }
        if (minute > 0L) {
            return String.format("%02d minutes, %02d seconds",
                    minute, second);
        }
        return String.format("%02d seconds", second);
    }

    // ripped from Shantay_Trader
    private String per_hour(long count, long time) {
        double amount, secs;
        if (count == 0) return "0";
        amount = count * 60.0 * 60.0;
        secs = (System.currentTimeMillis() - time) / 1000.0;
        return iformat.format(amount / secs);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            room = ch_room.getSelectedItem().equalsIgnoreCase("West") ? 1 : 0;
            useSupers = ch_useSupers.getSelectedItem().equalsIgnoreCase("yes") ? true : false;
            buryBones = ch_buryBones.getSelectedItem().equalsIgnoreCase("yes") ? true : false;
            try {
                eatAt = Integer.parseInt(tf_eatAt.getText().trim());
                eatAtEmergency = Integer.parseInt(tf_eatAtEmergency.getText().trim());
            } catch (Throwable t) {
                System.out.println("Error parsing config values");
                System.out.println(t);
            }
        }
        frame.setVisible(false);
    }
}
