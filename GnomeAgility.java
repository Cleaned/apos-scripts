/**
*  START AT THE END OF AN OBSTACLE!
*/
public class GnomeAgility extends Script {
 
  public GnomeAgility(Extension e)  {
  super(e);
  }
 
  public void init(String params){
  }
 
  public int main(){
     int[] obj = new int[]{-1,-1,-1};
     
     if(getFatigue() > 90) {
        useSleepingBag();
        return 1000;
     }
     if (getX() == 683 && getY() == 494) {
         obj = getObjectById(655); 
         atObject(obj[1], 495);
         return 2000;
     }
      if (getX() == 692 && getY() == 499) {
          obj = getObjectById(647);
          atObject(obj[1], obj[2]);
          return 2000;
      }
      if (getX() == 692 && getY() == 1448) {
          obj = getObjectById(648);
          atObject(obj[1], obj[2]);
          return 2000;
      }
      if (getX() == 693 && getY() == 2394) {
          obj = getObjectById(650);
          atObject(obj[1], obj[2]);
          return 2000;
      }
      if (getX() == 685 && getY() == 2396) {
          obj = getObjectById(649);
          atObject(obj[1], obj[2]);
          return 2000;
      }
      if (getX() == 683 && getY() == 506) {
          obj = getObjectById(653); 
          atObject(obj[1], obj[2]);
          return 2000;
      }
      if (getX() == 683 && getY() == 501) {
          obj = getObjectById(654); 
          atObject(obj[1], obj[2]);
          return 2000;
      }
     return 2000;
  }
}
