Updates to existing scripts to make them run better, or new scripts are going here. All scripts modified by me unless I've specified otherwise here:

**BilboBaggins** - The script that controls Bilbo Baggin in Lumbridge which gives a sleeping bag to any user that trades it. Only accepts coins in return, and will shop for more bags (with door detection). Writes and reads to/from file to track who has already received one. Should be for just educational purposes only.

**GnomeAgility** - an "improved" gnome agility course over the GnomeAgility distributed with APOS (meaning, it at least completes the course). Be sure to start it AFTER completing any obstacle... it's all based on exact coordinates, so if it fails to start just click on any of the obstacles.

**S_GnomeFlaxBank** - Credit to HelixSpiral. Modified the S_GnomeFlax script to target the flax below banks and banks them. Works flawlessly!

**S_TreeFletch** -  A minor update to the script to target trees closest to the player. This script still has a lot of flaws in it though for trees lower than Willow.

**FaladorSpinnerFlax** - Updated the FaladorSpinner script to handle flax, and improved some of the timing. Should be able to spin 2k/hr

**HighAlcher** - Modified S_HighAlcher to support Shantay chest banking. On spell fail, moves rebanks, and sleeps - then waits until 20 seconds have passed since failure before trying again

**ChestTrader** - Pretty quick Shantay Chest trader. Item IDs not required for the taker

**S_Pickpocket** - Updated to support burrying bones

**ZanarisCooker** - Cooks tuna, lobster, swordfish, or sharks in Zanaris (closest bank to range)

**CleanAllHerbs** - Cleans all herbs in your bank from lowest level to highest. Support normal banks and Shantay Chest

**FireGiants** - Requires 45 magic (air runes and law runes) for camelot teleport. Fights fire giants in waterfall dungeon supporting either room, bone burying, and super potions. Only uses sharks as food.

**S_Smelter** - Update to ore count sanity checks for better banking.

**WhiteBerries** - Uses 2 accounts to trade across the lava river in the wilderness to collect whiteberries. Make sure that both accounts have the same amount of free space available.
