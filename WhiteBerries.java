import com.aposbot.Constants;
import com.aposbot.StandardCloseHandler;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Locale;

public class WhiteBerries extends Script implements ActionListener {

    private Frame frame;
    private Choice ch_Give;
    private TextField tf_partner, tf_invSpaces;
    private PathWalker pw;
    private PathWalker.Path TO_BANK, TO_DRAGONS;
    private Point BANK = new Point(215,450);
    private Point DRAGONS = new Point(136, 221);
    private int WHITE_BERRIES = 471;
    private Point WHITE_BERRY_LOC = new Point(137,213);
    private String partner;
    private boolean give, trading, needToMove;
    private long startTime = -1;
    private int lastX = 0, lastY = 0, tradeQuantity = 30, bankedQuantity = 0, startQuantity = -1;
    private final DecimalFormat iformat = new DecimalFormat("#,##0");

    public WhiteBerries(Extension ex) {
        super(ex);
        pw = new PathWalker(ex);
    }

    @Override
    public void init(String params) {
        pw.init(null);
        if (frame == null) {
            Panel pInput = new Panel(new GridLayout(0, 2, 0, 2));
            pInput.add(new Label("Account Job:"));
            ch_Give = new Choice();
            ch_Give.add("Give");
            ch_Give.add("Take");
            pInput.add(ch_Give);

            pInput.add(new Label("Partner Name:"));
            pInput.add(tf_partner = new TextField());
            pInput.add(new Label("Free Inventory Spaces:"));
            pInput.add(tf_invSpaces = new TextField());

            Button button;
            Panel pButtons = new Panel();
            button = new Button("OK");
            button.addActionListener(this);
            pButtons.add(button);
            button = new Button("Cancel");
            button.addActionListener(this);
            pButtons.add(button);
            frame = new Frame(getClass().getSimpleName());
            frame.addWindowListener(
                    new StandardCloseHandler(frame, StandardCloseHandler.HIDE)
            );
            frame.setIconImages(Constants.ICONS);
            frame.add(pInput, BorderLayout.NORTH);
            frame.add(pButtons, BorderLayout.SOUTH);
            frame.setResizable(false);
            frame.pack();
        }
        frame.setLocationRelativeTo(null);
        frame.toFront();
        frame.requestFocus();
        frame.setVisible(true);
    }

    @Override
    public int main() {

        if (startTime == -1) {
            startTime = System.currentTimeMillis();
        }

        if (pw.walkPath()) {
            // fix for npc blocking
            // this can be replaced with a short return after fixed
            if (inCombat()) return 600;
            if (getX() == lastX && getY() == lastY) {
                for(int i = 0; i < 794; i++) {
                    int[] npc = getNpcInRadius(i, getX(), getY(), 1);
                    if (npc[0] != -1) {
                        System.out.println("Attacking " + getNpcNameId(i));
                        attackNpc(npc[0]);
                        return 1200;
                    }
                }
            } else {
                lastX = getX();
                lastY = getY();
                return 1800;
            }
        }

        if (isQuestMenu()) {
            answer(0);
            return 2000;
        }

        if (isBanking()) {
            if (startQuantity == -1) {
                startQuantity = bankCount(WHITE_BERRIES);
            }
            bankedQuantity = bankCount(WHITE_BERRIES) - startQuantity;
            if (getInventoryCount(WHITE_BERRIES) > 0) {
                deposit(WHITE_BERRIES, getInventoryCount(WHITE_BERRIES));
                return 1200;
            }
            closeBank();
            return 600;
        }

        if (isInTradeConfirm() || isInTradeOffer()) {
            if (isInTradeOffer()) {
                if (give) {
                    if (getLocalTradeItemCount() <= 0) {
                        offerItemTrade(getInventoryIndex(WHITE_BERRIES), Math.min(12, getInventoryCount(WHITE_BERRIES)));
                        return 1200;
                    }
                }
                acceptTrade();
                return 600;
            } else {
                confirmTrade();
                return 600;
            }
        }

        if (needToMove) {
            needToMove = false;
            if (isReachable(getX() + 1, getY())) {
                walkTo(getX()+1,getY());
                return 1500;
            }
            if (isReachable(getX(), getY()+1)) {
                walkTo(getX(),getY()+1);
                return 1500;
            }
            if (isReachable(getX(), getY()-1)) {
                walkTo(getX(),getY()-1);
                return 1500;
            }
            if (isReachable(getX()-1, getY())) {
                walkTo(getX()-1,getY());
                return 1500;
            }
        }

        if (!give) {
            if (getInventoryCount() >= tradeQuantity) {
                if (distanceTo(BANK.x, BANK.y) > 10) {
                    pw.setPath(TO_BANK);
                    return 1000;
                } else {
                    int[] bankers = getNpcByIdNotTalk(BANKERS);
                    if (bankers[0] != -1) {
                        talkToNpc(bankers[0]);
                        return 2000;
                    }
                }
            } else {
                if (distanceTo(BANK.x, BANK.y) <= 10) {
                    pw.setPath(TO_DRAGONS);
                    return 1000;
                }
                if (distanceTo(DRAGONS.x, DRAGONS.y) < 10) {
                    int[] player = getPlayerByName(partner);
                    if (player[0] != -1) {
                        System.out.println("Sending trade");
                        sendTradeRequest(getPlayerPID(player[0]));
                        return 2000;
                    } else {
                        trading = false;
                    }
                }
            }
        } else {
            if (getInventoryCount(WHITE_BERRIES) == 0) {
                trading = false;
            }
            if (trading || getInventoryCount() == tradeQuantity) {
                trading = true;
                if (getX() != 136 && getY() != 218) {
                    walkTo(136, 218);
                    return 2000;
                }
                int[] player = getPlayerByName(partner);
                if (player[0] != -1) {
                    System.out.println("Sending trade");
                    sendTradeRequest(getPlayerPID(player[0]));
                    return 2000;
                }
            }
            if (!trading) {
                pickupItem(WHITE_BERRIES, WHITE_BERRY_LOC.x, WHITE_BERRY_LOC.y);
                return 600;
            }
        }
        return 600;
    }

    @Override
    public void actionPerformed(ActionEvent aE) {
        if (aE.getActionCommand().equals("OK")) {
            partner = tf_partner.getText().trim();
            give = (ch_Give.getSelectedItem().equalsIgnoreCase("give")) ? true : false;
            tradeQuantity = Integer.parseInt(tf_invSpaces.getText().trim());
            if (!give) {
                TO_BANK = pw.calcPath(DRAGONS.x, DRAGONS.y, BANK.x, BANK.y);
                TO_DRAGONS = pw.calcPath(BANK.x, BANK.y, DRAGONS.x, DRAGONS.y);
            }
        }
        frame.setVisible(false);
    }

    @Override
    public void paint() {
        int y = 25;
        drawString("White Berries", 190, y, 1, 0xFFFFFF);
        y += 13;
        drawString("Runtime: " + get_time_since(startTime), 190, y, 1, 0xFFFFFF);
        y += 13;
        drawString("Partner: " + partner, 190, y, 1, 0xFFFFFF);
        y += 13;
        if (!give) {
            drawString("Banked: " + bankedQuantity, 190, y, 1, 0xFFFFFF);
            y += 13;
            drawString("Rate: " + per_hour(bankedQuantity, startTime) + " / hr", 190, y, 1, 0xFFFFFF);
        }

    }

    private static String get_time_since(long t) {
        long millis = (System.currentTimeMillis() - t) / 1000;
        long second = millis % 60;
        long minute = (millis / 60) % 60;
        long hour = (millis / (60 * 60)) % 24;
        long day = (millis / (60 * 60 * 24));

        if (day > 0L) {
            return String.format("%02d days, %02d hrs, %02d mins",
                    day, hour, minute);
        }
        if (hour > 0L) {
            return String.format("%02d hours, %02d mins, %02d secs",
                    hour, minute, second);
        }
        if (minute > 0L) {
            return String.format("%02d minutes, %02d seconds",
                    minute, second);
        }
        return String.format("%02d seconds", second);
    }

    // ripped from Shantay_Trader
    private String per_hour(long count, long time) {
        double amount, secs;

        if (count == 0) return "0";
        amount = count * 60.0 * 60.0;
        secs = (System.currentTimeMillis() - time) / 1000.0;
        return iformat.format(amount / secs);
    }

    @Override
    public void onServerMessage(String str) {
        str = str.toLowerCase();
        if (str.contains("have been standing")) {
            needToMove = true;
        }
    }
}
